SHELL = /bin/sh
PREVIEW_APP_NAME = vue3app-preview
IMAGE_NAME = vue3app

DOCKER_BIN = $(shell command -v docker 2> /dev/null)

build-image:
	$(DOCKER_BIN) build -f ./docker/Dockerfile . -t $(IMAGE_NAME)

preview-up:
	@printf "\n   \e[30;43m %s \033[0m\n\n" 'Server started: <http://127.0.0.1:80>'
	$(DOCKER_BIN) run -d -p 80:80 $(IMAGE_NAME)
