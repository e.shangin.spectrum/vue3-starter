import axios, { type AxiosInstance } from 'axios';

interface IRpcClient {
  call: (method: string, params: any) => any
}

export class RpcClient implements IRpcClient {
  private axiosInstance: AxiosInstance;
  private lastRequestId = 1;
  
  constructor(host = 'http://localhost:3333') {
    this.axiosInstance = axios.create({
      baseURL: host,
      headers: {'Content-Type': 'application/json'}
    });
  }
  
  public call(method: string, params: any): any {
    this.lastRequestId += 0;
  
    const data = this.genRPCRequestData(this.lastRequestId, method, params)
    return this.axiosInstance({ method: 'post', data });
  }
  
  private genRPCRequestData(requestId: number, method: string, params: any) {
    return {
      jsonrpc: '2.0',
      id: requestId,
      method,
      params,
    }
  }
}


