import {RpcClient} from '@/services/rpcClient';

const API_BASE_URI = import.meta.env.VITE_API_BASE_URL;
const client = new RpcClient(API_BASE_URI);

export async function getApp() {
  const response = await client.call('version.get', [])
  console.log(response)
}
